-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 22 Okt 2020 pada 16.31
-- Versi server: 5.7.24
-- Versi PHP: 7.3.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siapbps`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bentuk`
--

CREATE TABLE IF NOT EXISTS `bentuk` (
  `id_bentuk` varchar(10) NOT NULL,
  `nama_bentuk` varchar(50) NOT NULL,
  `deleted` varchar(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_bentuk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bentuk`
--

INSERT INTO `bentuk` (`id_bentuk`, `nama_bentuk`, `deleted`, `created_at`, `modified_at`) VALUES
('BTK00001', 'Pil', '0', '2020-07-12 22:20:38', NULL),
('BTK00002', 'Cair', '0', '2020-07-12 22:20:50', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bidan`
--

CREATE TABLE IF NOT EXISTS `bidan` (
  `nip_bidan` varchar(50) NOT NULL,
  `nama_bidan` varchar(100) DEFAULT NULL,
  `telp_bidan` varchar(20) DEFAULT NULL,
  `deleted` varchar(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`nip_bidan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bidan`
--

INSERT INTO `bidan` (`nip_bidan`, `nama_bidan`, `telp_bidan`, `deleted`, `created_at`, `modified_at`) VALUES
('admin', 'admin', 'telp1', '0', '2020-07-12 22:07:10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `desa`
--

CREATE TABLE IF NOT EXISTS `desa` (
  `kode_desa` varchar(10) NOT NULL,
  `nama_desa` varchar(50) NOT NULL,
  `deleted` varchar(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kode_desa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `desa`
--

INSERT INTO `desa` (`kode_desa`, `nama_desa`, `deleted`, `created_at`, `modified_at`) VALUES
('01', 'gondang', '0', '2020-10-22 23:11:00', NULL),
('DS00001', 'DESA1', '0', '2020-07-10 22:57:31', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kartu_keluarga`
--

CREATE TABLE IF NOT EXISTS `kartu_keluarga` (
  `no_kk` varchar(50) NOT NULL,
  `alamat` text,
  `deleted` varchar(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`no_kk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kartu_keluarga`
--

INSERT INTO `kartu_keluarga` (`no_kk`, `alamat`, `deleted`, `created_at`, `modified_at`) VALUES
('1234567891012234', '123456', '0', '2020-07-19 23:51:44', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` varchar(10) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `deleted` varchar(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`, `deleted`, `created_at`, `modified_at`) VALUES
('KTG000001', 'Kategori1', '0', '2020-07-12 22:26:43', NULL),
('KTG000002', 'Kategori2', '0', '2020-07-12 22:26:43', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `obat`
--

CREATE TABLE IF NOT EXISTS `obat` (
  `id_obat` varchar(10) NOT NULL,
  `id_bentuk` varchar(10) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `nama_obat` varchar(50) NOT NULL,
  `dosis` varchar(50) NOT NULL,
  `deleted` varchar(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_obat`),
  KEY `id_bentuk` (`id_bentuk`),
  KEY `id_kategori` (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `obat`
--

INSERT INTO `obat` (`id_obat`, `id_bentuk`, `id_kategori`, `nama_obat`, `dosis`, `deleted`, `created_at`, `modified_at`) VALUES
('OBT00001', 'BTK00002', 'KTG000001', 'Mingrip1', 'Permanent1', '0', '2020-07-18 22:29:45', '2020-07-18 10:30:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE IF NOT EXISTS `pasien` (
  `no_urut` varchar(50) NOT NULL,
  `nama_pasien` varchar(30) NOT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL COMMENT 'L = Laki-laki, P = Perempuan',
  `status_hubungan` varchar(20) DEFAULT NULL,
  `no_telp_pasien` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  `no_kk` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`no_urut`),
  KEY `pasien_fk` (`no_kk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `posyandu`
--

CREATE TABLE IF NOT EXISTS `posyandu` (
  `id_posyandu` varchar(10) NOT NULL,
  `nama_posyandu` varchar(100) NOT NULL,
  `alamat_posyandu` text NOT NULL,
  `telp_posyandu` varchar(20) DEFAULT NULL,
  `deleted` varchar(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_posyandu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `posyandu`
--

INSERT INTO `posyandu` (`id_posyandu`, `nama_posyandu`, `alamat_posyandu`, `telp_posyandu`, `deleted`, `created_at`, `modified_at`) VALUES
('PYD00001', 'Posyandu1', 'Alamat1', '0831', '0', '2020-07-12 01:44:03', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pengguna` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `deleted` varchar(1) NOT NULL DEFAULT '0',
  `tipe_pengguna` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `id_pengguna`, `nama`, `email`, `password`, `deleted`, `tipe_pengguna`, `created_at`, `modified_at`) VALUES
(1, 'admin', 'Admin', 'admin@siapbps.com', '$2y$10$0/iqR28qFMLByIAnNon5tuLf7Uj26L2pB9ragYRytBAgb.0fM4MpS', '0', 'admin', '2020-10-22 20:48:42', NULL);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `obat_ibfk_1` FOREIGN KEY (`id_bentuk`) REFERENCES `bentuk` (`id_bentuk`),
  ADD CONSTRAINT `obat_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`);

--
-- Ketidakleluasaan untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD CONSTRAINT `pasien_fk` FOREIGN KEY (`no_kk`) REFERENCES `kartu_keluarga` (`no_kk`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
