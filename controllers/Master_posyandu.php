<?php
include "models/Master_posyandu_model.php";

function load_data()
{
    $result = json_decode(get_data_table());
    if ($result->status) {
        $no = 0;
        foreach ($result->data as $model) {
            $no++;
            $row = array();
            $row = get_object_vars($model);
            $row['no'] = $no;
            $data_to_display[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $result->total,
            "recordsFiltered" => $result->filtered,
            "data" => $data_to_display,
            "status_code" => 200,
            "status_message" => "success"
        );
    } else {
        $output = array(
            "data" => [],
            "recordsTotal" => 0,
            "recordsFiltered" => 0,
            "status_code" => 500,
            "status_message" => "Data Tidak Tersedia",
        );
    }
    return json_encode($output);
}

function simpan($data)
{
    $data = json_decode($data);
    $response = array();

    if (!empty($data->id_posyandu)) {
        $result = update_data($data);
    } else {
        $result = simpan_data($data);
    }

    if ($result) {
        $response["status_data"] = $result;
        $response["status_code"] = 200;
        $response["status_message"] = $result == -1 ? "Data Sudah Ada" : "Simpan Berhasil";
    } else {
        $response["status_data"] = $result;
        $response["status_code"] = 500;
    }

    return json_encode($response);
}

function hapus($data)
{
    $data = json_decode($data);
    if (delete_data(array("deleted"=>(int) !$data->deleted), $data->id)) {
        $response["status_data"] = true;
        $response["status_code"] = 200;
        $response["status_message"] = "Data Berhasil Dihapus";
    } else {
        $response["status_data"] = false;
        $response["status_code"] = 500;
    }

    return json_encode($response);
}

function get_combobox($query)
{
    return get_data_combobox($query);
}
