<?php
include "models/Master_user.php";

function login_verify($data)
{
    $user = get_user_by_email($data["email"]);
    if (!$user) {
        $_SESSION["flash"] = "User Tidak Ditemukan";
        return false;
    }
    if (password_verify($data["password"], $user["password"])) {
        $_SESSION["user"] = $user["id"];
        $_SESSION["id_pengguna"] = $user["id_pengguna"];
        $_SESSION["nama"] = $user["nama"];
        $_SESSION["email"] = $user["email"];
        $_SESSION["tipe_pengguna"] = $user["tipe_pengguna"];
    } else {
        $_SESSION["flash"] = "Password salah";
        return false;
    }

    return true;
}
