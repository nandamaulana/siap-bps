<?php
include "CRUD.php";
function get_data_table()
{
    $table = "bidan b";
    $join = " join posyandu p on (p.id_posyandu=b.id_posyandu) ";
    $join .= " join desa d on (d.kode_desa=p.id_desa) ";
    $like = "";
    $order_by = "";
    $column_search_order = array('nip_bidan', 'nip_bidan', 'nama_bidan', 'nama_desa', 'nama_posyandu', 'telp_bidan');
    if (!empty($_POST['search']['value'])) {
        foreach ($column_search_order as $index => $column) {
            if ($index === 0) {
                $like .= "$column LIKE '%" . $_POST['search']['value'] . "%'";
            } else {
                $like .= "OR $column LIKE '%" . $_POST['search']['value'] . "%'";
            }
        }
    }
    if (isset($_POST['order']['0']['column']) && isset($_POST['order']['0']['dir'])) {
        $order_by = $column_search_order[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
    }
    $limit = $_POST['length'] . " OFFSET " . $_POST['start'];
    $result = read("*, b.deleted deleted_status, b.id id_bidan", $table, $join, $like, $order_by, $limit);
    if ($result->num_rows) {
        $response["status"] = true;
        $response["data"] = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $response["filtered"] = get_filtered($table, $join, $like, $order_by, $limit);
        $response["total"] = get_total($table, $join);
    } else {
        $response["status"] = false;
        $response["data"] = [];
    }
    return json_encode($response);
}

function get_filtered($table, $join, $like, $order_by, $limit)
{
    return read("*", $table, $join, $like, $order_by, $limit)->num_rows;
}

function get_total($table, $join)
{
    return read("*", $table, $join, "")->num_rows;
}

function simpan_data($data)
{
    $table = "bidan";
    if (check_nip_bidan($data, $table)) {
        return -1;
    }
    return insert((array) $data, $table);
}

function update_data($data)
{
    $table = "bidan";
    $data->modified_at = date("Y-m-d h:i:s");
    return update((array) $data, $table, "nip_bidan = '" . $data->nip_bidan . "'");
}

function delete_data($data, $id)
{
    return update($data, 'bidan', "id = '$id'");
}

function check_nip_bidan($data, $table)
{
    return read("*", $table, "", "nip_bidan = '" . $data->nip_bidan . "'")->num_rows;
}
