<?php

include 'config/connection.php';

function insert($data = array(), $table)
{
    $conn = getConnection();
    return $conn->query(buildInsertQuery($data, $table));
}

function read($field, $table, $join = "", $where = "", $order_by = "", $limit = "")
{
    $conn = getConnection();
    $query = "SELECT $field FROM $table "
            .($join != "" ? " $join " : "")
            .($where != "" ? " WHERE $where " : "")
            .($order_by != "" ? " ORDER BY $order_by " : "")
            .($limit != "" ? " LIMIT $limit " : "");
    // echo $query;
    return mysqli_query($conn, $query);
}

function update($data = array(), $table, $where = null)
{
    $conn = getConnection();
    return mysqli_query($conn, buildUpdateQuery($data, $table, $where));
}

function delete($table, $where = null)
{
    $conn = getConnection();
    return mysqli_query($conn, buildDeleteQuery($table, $where));
}

function buildInsertQuery($arrayInsert = array(), $table)
{
    $query = "INSERT INTO $table ";
    $field = "(";
    $values = "VALUES(";
    foreach ($arrayInsert as $key => $value) {
        $field .= $key.", ";
        $values .= "'".$value."'".", ";
        // $values .= (is_numeric($value) ? $value  : "'".$value."'").", ";
    }
    $field = rtrim(trim($field), ',').") ";
    $values = rtrim(trim($values), ',');
    $query .= $field;
    $query .= $values.")";
    // echo $query;
    return $query;
}

function buildUpdateQuery($arrayUpdate = array(), $table, $where)
{
    $query = "UPDATE $table SET";
    foreach ($arrayUpdate as $key => $value) {
        $query .= " $key = '$value',";
    }
    $query = rtrim(trim($query), ',');

    $query .= " WHERE $where";
    // echo $query;
    return $query;
}

function buildDeleteQuery($table, $where)
{
    return "DELETE FROM $table WHERE $where";
}
