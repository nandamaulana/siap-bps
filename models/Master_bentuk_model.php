<?php
include "CRUD.php";
function get_data_table()
{
    $table = "bentuk";
    $join = "";
    $like = "";
    $order_by = "";
    $column_search_order = array('id_bentuk', 'id_bentuk', 'nama_bentuk');
    if (!empty($_POST['search']['value'])) {
        foreach ($column_search_order as $index => $column) {
            if ($index === 0) {
                $like .= "AND $column LIKE '%" . $_POST['search']['value'] . "%'";
            } else {
                $like .= "OR $column LIKE '%" . $_POST['search']['value'] . "%'";
            }
        }
    }
    if (isset($_POST['order']['0']['column']) && isset($_POST['order']['0']['dir'])) {
        $order_by = $column_search_order[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
    }
    $limit = $_POST['length'] . " OFFSET " . $_POST['start'];
    $result = read("*", $table, $join, $like, $order_by, $limit);
    if ($result->num_rows) {
        $response["status"] = true;
        $response["data"] = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $response["filtered"] = get_filtered($table, $join, $like, $order_by, $limit);
        $response["total"] = get_total($table, $join);
    } else {
        $response["status"] = false;
        $response["data"] = [];
    }
    return json_encode($response);
}

function get_filtered($table, $join, $like, $order_by, $limit)
{
    return read("*", $table, $join, $like, $order_by, $limit)->num_rows;
}

function get_total($table, $join)
{
    return read("*", $table, $join)->num_rows;
}

function get_data_combobox($query)
{
    $result = read("id_bentuk id, nama_bentuk field", "bentuk", "", "deleted = '0' AND nama_bentuk LIKE '%$query%'");
    $response["data"] = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return json_encode($response);
}

function simpan_data($data)
{
    $table = "bentuk";
    if (check_nama_bentuk($data, $table)) {
        return -1;
    }
    $data->id_bentuk = "BTK".str_pad(get_id($table), 5, "0", STR_PAD_LEFT);
    return insert((array) $data, $table);
}

function update_data($data)
{
    $table = "bentuk";
    if (check_nama_bentuk($data, $table)) {
        return -1;
    }
    $data->modified_at = date("Y-m-d h:i:s");
    return update((array) $data, $table, "id_bentuk = '".$data->id_bentuk."'");
}

function delete_data($data, $id)
{
    return update($data, "bentuk", "id_bentuk = '$id'");
}

function check_nama_bentuk($data, $table)
{
    return read("*", $table, "", "deleted = '0' AND nama_bentuk = '".$data->nama_bentuk."'")->num_rows;
}

function get_id($table)
{
    return mysqli_fetch_assoc(read("count(*) jumlah", $table, "", "deleted = '0'"))['jumlah']+1;
}
