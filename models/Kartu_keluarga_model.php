<?php
include "CRUD.php";
function get_data_table()
{
    $table = "kartu_keluarga";
    $join = "";
    $like = "";
    $order_by = "";
    $column_search_order = array('no_kk', 'no_kk', 'alamat');
    if (!empty($_POST['search']['value'])) {
        foreach ($column_search_order as $index => $column) {
            if ($index === 0) {
                $like .= "AND $column LIKE '%" . $_POST['search']['value'] . "%'";
            } else {
                $like .= "OR $column LIKE '%" . $_POST['search']['value'] . "%'";
            }
        }
    }
    if (isset($_POST['order']['0']['column']) && isset($_POST['order']['0']['dir'])) {
        $order_by = $column_search_order[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
    }
    $limit = $_POST['length'] . " OFFSET " . $_POST['start'];
    $result = read("*", $table, $join, "deleted = '0' $like", $order_by, $limit);
    if ($result->num_rows) {
        $response["status"] = true;
        $response["data"] = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $response["filtered"] = get_filtered($table, $join, $like, $order_by, $limit);
        $response["total"] = get_total($table, $join);
    } else {
        $response["status"] = false;
        $response["data"] = [];
    }
    return json_encode($response);
}

function get_filtered($table, $join, $like, $order_by, $limit)
{
    return read("*", $table, $join, "deleted = '0' $like", $order_by, $limit)->num_rows;
}

function get_total($table, $join)
{
    return read("*", $table, $join, "deleted = '0'")->num_rows;
}

function simpan_data($data)
{
    $table = "kartu_keluarga";
    if (check_no_kk($data, $table)) {
        return -1;
    }
    
    return insert((array) $data, $table);
}

function update_data($data)
{
    $table = "kartu_keluarga";
    $data->modified_at = date("Y-m-d h:i:s");
    return update((array) $data, $table, "no_kk = '" . $data->no_kk . "'");
}

function delete_data($id)
{
    return delete("kartu_keluarga", "no_kk = '$id'");
}

function check_no_kk($data, $table)
{
    return read("*", $table, "", "deleted = '0' AND no_kk = '" . $data->no_kk . "'")->num_rows;
}