<?php
include "CRUD.php";
function get_data_table()
{
    $table = "desa";
    $join = "";
    $like = "";
    $order_by = "";
    $column_search_order = array('kode_desa', 'kode_desa', 'nama_desa');
    if (!empty($_POST['search']['value'])) {
        foreach ($column_search_order as $index => $column) {
            if ($index === 0) {
                $like .= "AND $column LIKE '%" . $_POST['search']['value'] . "%'";
            } else {
                $like .= "OR $column LIKE '%" . $_POST['search']['value'] . "%'";
            }
        }
    }
    if (isset($_POST['order']['0']['column']) && isset($_POST['order']['0']['dir'])) {
        $order_by = $column_search_order[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
    }
    $limit = $_POST['length'] . " OFFSET " . $_POST['start'];
    $result = read("*", $table, $join, $like, $order_by, $limit);
    if ($result->num_rows) {
        $response["status"] = true;
        $response["data"] = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $response["filtered"] = get_filtered($table, $join, $like, $order_by, $limit);
        $response["total"] = get_total($table, $join);
    } else {
        $response["status"] = false;
        $response["data"] = [];
    }
    return json_encode($response);
}

function get_filtered($table, $join, $like, $order_by, $limit)
{
    return read("*", $table, $join, $like, $order_by, $limit)->num_rows;
}

function get_total($table, $join)
{
    return read("*", $table, $join)->num_rows;
}

function simpan_data($data, $manual = false)
{
    $table = "desa";
    if (check_nama_desa($data, $table)) {
        return -1;
    }
    if (!$manual) {
        $data->kode_desa = "DS" . str_pad(get_id($table), 5, "0", STR_PAD_LEFT);
    }
    return insert((array) $data, $table);
}

function update_data($data)
{
    $table = "desa";
    $data->modified_at = date("Y-m-d h:i:s");
    if (check_nama_desa($data, $table)) {
        return -1;
    }
    return update((array) $data, $table, "kode_desa = '" . $data->kode_desa . "'");
}

function delete_data($data, $id)
{
    return update($data, "desa", "kode_desa = '".$id."'");
}

function check_nama_desa($data, $table)
{
    return read("*", $table, "", "deleted = '0' AND nama_desa = '" . $data->nama_desa . "'")->num_rows;
}

function get_id($table)
{
    return mysqli_fetch_assoc(read("count(*) jumlah", $table, ""))['jumlah'] + 1;
}

function get_data_combobox($query)
{
    $result = read("kode_desa id, nama_desa field", "desa", "", "deleted = '0' AND nama_desa LIKE '%$query%'");
    $response["data"] = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return json_encode($response);
}
