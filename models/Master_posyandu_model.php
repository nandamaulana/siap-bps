<?php
include "CRUD.php";
function get_data_table()
{
    $table = "posyandu";
    $join = "join desa on (desa.kode_desa = posyandu.id_desa)";
    $like = "";
    $order_by = "";
    $column_search_order = array('id_posyandu', 'id_posyandu', 'desa.nama_desa', 'nama_posyandu', 'alamat_posyandu', 'telp_posyandu');
    if (!empty($_POST['search']['value'])) {
        foreach ($column_search_order as $index => $column) {
            if ($index === 0) {
                $like .= "AND $column LIKE '%" . $_POST['search']['value'] . "%'";
            } else {
                $like .= "OR $column LIKE '%" . $_POST['search']['value'] . "%'";
            }
        }
    }
    if (isset($_POST['order']['0']['column']) && isset($_POST['order']['0']['dir'])) {
        $order_by = $column_search_order[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
    }
    $limit = $_POST['length'] . " OFFSET " . $_POST['start'];
    $result = read("posyandu.*, desa.kode_desa, desa.nama_desa", $table, $join, $like, $order_by, $limit);
    if ($result->num_rows) {
        $response["status"] = true;
        $response["data"] = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $response["filtered"] = get_filtered($table, $join, $like, $order_by, $limit);
        $response["total"] = get_total($table, $join);
    } else {
        $response["status"] = false;
        $response["data"] = [];
    }
    return json_encode($response);
}

function get_filtered($table, $join, $like, $order_by, $limit)
{
    return read("*", $table, $join, $like, $order_by, $limit)->num_rows;
}

function get_total($table, $join)
{
    return read("*", $table, $join)->num_rows;
}

function simpan_data($data)
{
    $table = "posyandu";
    if (check_nama_posyandu($data, $table)) {
        return -1;
    }
    $data->id_posyandu = "PYD" . str_pad(get_id($table), 5, "0", STR_PAD_LEFT);
    return insert((array) $data, $table);
}

function update_data($data)
{
    $table = "posyandu";
    if (check_nama_posyandu($data, $table)) {
        return -1;
    }
    $data->modified_at = date("Y-m-d h:i:s");
    return update((array) $data, $table, "id_posyandu = '" . $data->id_posyandu . "'");
}

function delete_data($data, $id)
{
    return update($data, "posyandu", "id_posyandu = '".$id."'");
}

function check_nama_posyandu($data, $table)
{
    return read("*", $table, "", "deleted = '0' AND nama_posyandu = '" . $data->nama_posyandu . "'")->num_rows;
}

function get_id($table)
{
    return mysqli_fetch_assoc(read("count(*) jumlah", $table, "", "deleted = '0'"))['jumlah'] + 1;
}

function get_data_combobox($query)
{
    $result = read("id_posyandu id, concat(nama_posyandu, ' (', nama_desa, ')') field", "posyandu p", "join desa d on (p.id_desa = d.kode_desa )", "p.deleted = '0' AND concat(nama_posyandu, ' (', nama_desa, ')') LIKE '%$query%'");
    $response["data"] = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return json_encode($response);
}
