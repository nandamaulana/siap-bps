<?php
date_default_timezone_set("Asia/Jakarta");
function getConnection()
{
    static $conn;
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbName = "siapbps";
    if ($conn === null) {
        $conn = mysqli_connect($servername,$username,$password,$dbName);
    }
    return $conn;
}