var main_js = {
    baseUrl: null,
    ajax: function(url,kirim_data=null,callback,callback_error=null){
        main.block();
        $.ajax({
            type: "POST",
            url: main.baseUrl + url,
            data: {data_send: kirim_data},
            cache: false,
            dataType: "text",
            success: function(msg){
                callback(msg);
            },
            error: function(msg){
                if(callback_error!=null){
                    callback_error();
                }
            }
        });
    },
    selectize_remote: function(id, url, valueField = "id", labelField = "field", searchField = "field"){
        return $(id).selectize({
            valueField: valueField,
            labelField: labelField,
            searchField: searchField,
            preload: true,
            load: function(query, callback) {
                console.log(query)
                // if (!query.length) return callback();
                $.ajax({
                    url: main.baseUrl + url + encodeURIComponent(!query.length ? "ALL" : query),
                    type: 'GET',
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        res = JSON.parse(res)
                        callback(res.data.slice(0, 10));
                    }
                });
            },
        });
    },
    notifikasi: function(status_color, message) {
        $(".alert_div").html(' <div class="alert alert-'+status_color+' d-flex" role="alert" style="font-size:15px;">\
                            <span >' + message +'</span>\
                            <button class="close ml-auto" data-dismiss="alert">x</button>\
                            </div>').hide().fadeIn("slow");
    },
    block: function(){
        $.blockUI({ message: '<h3> <img src="'+main_js.baseUrl+'assets/img/progress.svg" />  silahkan tunggu . . . </h3>',
              css: {  border: 'none',
                      padding: '5px',
                      backgroundColor: 'white',
                      '-webkit-border-radius': '10px',
                      '-moz-border-radius': '10px',
                      opacity: 1
            },
            baseZ: 10080
          });
    },
    unblock: function () {
        $.unblockUI();
    },
    delay: (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })(),
    init: function (base_url) {
        main_js.baseUrl = base_url;
        main_js.settings_table_server_side = {
            "iDisplayLength": 10,
            "dom": "rtip",
            "destroy": true,
            "columnDefs": [
                {
                  "targets": 'no-sort',
                  "orderable": false
                }
            ],
            "language": {
              "emptyTable": "Tidak Ada Data Pada Tabel",
              "info": "Menampilkan <b>_START_ Sampai _END_</b> Data Dari _TOTAL_ Total Data",
              "processing": "<img width='25' src='"+main_js.baseUrl+"assets/img/progress.svg'> Sedang Loading Data...",
              "infoFiltered": "(Difilter Dari _MAX_ Data)",
              "infoEmpty": "Menampilkan <b> 0 Sampai 0</b> Data Dari _MAX_ Total Data",
              "zeroRecords": "Tidak ditemukan filter pencarian"
            },
            "processing": true,
            "serverSide": true,
            "initComplete": function(settings, json) {

            }
        };
    }
}
