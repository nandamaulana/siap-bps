<?php
session_start();
use Steampixel\Route;

include 'parts/Route.php';
// untuk localhost
// define('BASEPATH', '/SIAP-BPS/');
// define('BASE_URL', "http://localhost:8080/siap-bps/");
define('BASEPATH', '');
define('BASE_URL', "https://siap-bps.dev/");

function buildPage($title = '', $pages, $data =  array())
{
    include "parts/main.php";
}
// untuk localhost
// Route::add('', function () {
// untuk virtual host
Route::add('/', function () {
    if (empty($_SESSION["user"])) {
        include "pages/login.php";
        unset($_SESSION["flash"]);
    } else {
        header('Location: '.BASE_URL."admin/page/dashboard");
    }
});

Route::add('/login/verify', function () {
    require_once("controllers/Login.php");
    if (login_verify($_POST)) {
        if ($_SESSION["tipe_pengguna"] == "admin") {
            header('Location: '.BASE_URL."admin/page/dashboard");
        }
    } else {
        header('Location: '.BASE_URL);
    }
}, 'post');

Route::add('/logout', function () {
    session_unset();
    header('Location: '.BASE_URL);
});

Route::add('/admin/page/dashboard', function () {
    buildPage("Dashboard", "pages/admin/dashboard.php");
});

Route::add('/admin/page/master_desa', function () {
    buildPage("Master Desa", "pages/admin/master_desa.php");
});

Route::add('/master_desa/get_combobox/([A-Za-z0-9 ]*)', function ($query) {
    require_once("controllers/master_desa.php");
    echo get_combobox($query != "ALL" ? $query : "");
}, 'get');

Route::add('/admin/page/master_posyandu', function () {
    buildPage("Master Posyandu", "pages/admin/master_posyandu.php");
});

Route::add('/admin/page/master_bidan', function () {
    buildPage("Master Bidan", "pages/admin/master_bidan.php");
});

Route::add('/admin/page/master_bentuk', function () {
    buildPage("Master Bentuk", "pages/admin/master_bentuk.php");
});

Route::add('/admin/page/master_kategori', function () {
    buildPage("Master Kategori", "pages/admin/master_kategori.php");
});

Route::add('/admin/page/master_obat', function () {
    buildPage("Master Obat", "pages/admin/master_obat.php");
});

Route::add('/admin/page/kartu_keluarga', function () {
    buildPage("Kartu Keluarga", "pages/pasien/kartu_keluarga.php");
});

Route::add('/admin/page/pasien', function () {
    buildPage("Pasien", "pages/pasien/pasien.php");
});

Route::add('/admin/page/pemeriksaan_hamil', function () {
    buildPage("Pemeriksaan Hamil", "pages/layanan/pemeriksaan_hamil.php");
});

//model route
//route desa
Route::add('/master_desa/load_data', function () {
    require_once("controllers/Master_desa.php");
    echo load_data();
}, 'post');

Route::add('/master_desa/simpan', function () {
    require_once("controllers/Master_desa.php");
    echo simpan($_POST['data_send']);
}, 'post');

Route::add('/master_desa/delete', function () {
    require_once("controllers/Master_desa.php");
    echo hapus($_POST['data_send']);
}, 'post');

//route posyandu
Route::add('/master_posyandu/load_data', function () {
    require_once("controllers/Master_posyandu.php");
    echo load_data();
}, 'post');

Route::add('/master_posyandu/simpan', function () {
    require_once("controllers/Master_posyandu.php");
    echo simpan($_POST['data_send']);
}, 'post');

Route::add('/master_posyandu/delete', function () {
    require_once("controllers/Master_posyandu.php");
    echo hapus($_POST['data_send']);
}, 'post');

Route::add('/master_posyandu/get_combobox/([A-Za-z0-9 ]*)', function ($query) {
    require_once("controllers/master_posyandu.php");
    echo get_combobox($query != "ALL" ? $query : "");
}, 'get');

// bidan
Route::add('/master_bidan/load_data', function () {
    require_once("controllers/Master_bidan.php");
    echo load_data();
}, 'post');

Route::add('/master_bidan/simpan', function () {
    require_once("controllers/Master_bidan.php");
    echo simpan($_POST['data_send']);
}, 'post');

Route::add('/master_bidan/delete', function () {
    require_once("controllers/Master_bidan.php");
    echo hapus($_POST['data_send']);
}, 'post');

// bentuk
Route::add('/master_bentuk/load_data', function () {
    require_once("controllers/Master_bentuk.php");
    echo load_data();
}, 'post');

Route::add('/master_bentuk/simpan', function () {
    require_once("controllers/Master_bentuk.php");
    echo simpan($_POST['data_send']);
}, 'post');

Route::add('/master_bentuk/delete', function () {
    require_once("controllers/Master_bentuk.php");
    echo hapus($_POST['data_send']);
}, 'post');

Route::add('/master_bentuk/get_combobox/([A-Za-z0-9]*)', function ($query) {
    require_once("controllers/master_bentuk.php");
    echo get_combobox($query != "ALL" ? $query : "");
}, 'get');

//kategori

Route::add('/master_kategori/load_data', function () {
    require_once("controllers/Master_kategori.php");
    echo load_data();
}, 'post');

Route::add('/master_kategori/simpan', function () {
    require_once("controllers/Master_kategori.php");
    echo simpan($_POST['data_send']);
}, 'post');

Route::add('/master_kategori/delete', function () {
    require_once("controllers/Master_kategori.php");
    echo hapus($_POST['data_send']);
}, 'post');

Route::add('/master_kategori/get_combobox/([A-Za-z0-9]*)', function ($query) {
    require_once("controllers/master_kategori.php");
    echo get_combobox($query != "ALL" ? $query : "");
}, 'get');

//obat

Route::add('/master_obat/load_data', function () {
    require_once("controllers/master_obat.php");
    echo load_data();
}, 'post');

Route::add('/master_obat/simpan', function () {
    require_once("controllers/Master_obat.php");
    echo simpan($_POST['data_send']);
}, 'post');

Route::add('/master_obat/delete', function () {
    require_once("controllers/Master_obat.php");
    echo hapus($_POST['data_send']);
}, 'post');

// kartu keluarga
Route::add('/kartu_keluarga/load_data', function () {
    require_once("controllers/Kartu_keluarga.php");
    echo load_data();
}, 'post');

Route::add('/kartu_keluarga/simpan', function () {
    require_once("controllers/Kartu_keluarga.php");
    echo simpan($_POST['data_send']);
}, 'post');

Route::add('/kartu_keluarga/delete', function () {
    require_once("controllers/Kartu_keluarga.php");
    echo hapus($_POST['data_send']);
}, 'post');

//pasien
Route::add('/pasien/load_data', function () {
    require_once("controllers/Kartu_keluarga.php");
    echo load_data();
}, 'post');

Route::pathNotFound(function ($path) {
    echo $path;
    lost();
});

function lost()
{
    header('HTTP/1.0 404 Not Found');
    include "parts/404.php";
}

Route::methodNotAllowed(function ($path, $method) {
    header('HTTP/1.0 405 Method Not Allowed');
    include "parts/405.php";
});

//list route untuk ajax/ request

Route::run(BASEPATH);
