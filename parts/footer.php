<footer class="footer footer-transparent">
    <div class="container">
        <div class="row text-center align-items-center flex-row-reverse">
            <div class="col-lg-auto ml-lg-auto">
                <ul class="list-inline list-inline-dots mb-0">
                    <li class="list-inline-item"><a href="<?= BASE_URL ?>about" class="link-secondary">About</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                Novita © 2020
                All rights reserved.
            </div>
        </div>
    </div>
</footer>
<!-- Libs JS -->
<script src="<?= BASE_URL ?>assets/plugins/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?= BASE_URL ?>assets/plugins/apexcharts/dist/apexcharts.min.js"></script>
<script src="<?= BASE_URL ?>assets/plugins/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="<?= BASE_URL ?>assets/plugins/flatpickr/dist/flatpickr.min.js"></script>
<!-- Tabler Core -->
<script src="<?= BASE_URL ?>assets/js/tabler.min.js"></script>
<script>
    document.body.style.display = "block"

    $(document).ready(function(){
        $("a[href='#']").on('click',function(e){
            e.preventDefault();
        })
    });
</script>
