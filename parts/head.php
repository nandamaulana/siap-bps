<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title><?= $title ?> - Admin</title>
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <meta name="msapplication-TileColor" content="#206bc4" />
    <meta name="theme-color" content="#206bc4" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta name="robots" content="noindex,nofollow,noarchive" />
    <!-- <link rel="icon" href="./favicon.ico" type="image/x-icon"/> -->
    <!-- <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon"/> -->
    <!-- CSS files -->
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>assets/plugins/jqvmap/dist/jqvmap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>assets/css/tabler.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>assets/css/tabler-template.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>assets/plugins/datatables/datatables.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>assets/plugins/selectize/dist/css/selectize.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>assets/plugins/flatpickr/dist/flatpickr.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>assets/plugins/fontawesome/css/all.css" />

    <script type="text/javascript" src="<?= BASE_URL ?>assets/plugins/jquery/dist/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>assets/plugins/jquery/dist/jquery.blockUI.min.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>assets/plugins/jquery-validation/js/localization/messages_id.min.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>assets/plugins/peity/jquery.peity.min.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>assets/plugins/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>assets/plugins/selectize/dist/js/standalone/selectize.min.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>assets/js/main.js"></script>


    <style>
        body {
            display: none;
        }
        .error {
            color: red;
        }
    </style>
</head>
