<!DOCTYPE html>
<html lang="en">

<?php include "head.php" ?>

<body class="antialiased">
    <div class="page">
        <?php include "header.php" ?>
        <?php include "navbar.php" ?>
        <?php include $pages ?>
        <?php include "footer.php" ?>
    </div>
    <div class="modal modal-blur fade" id="modal_hapus" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-title"></div>
                    <div class="message"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link link-secondary mr-auto negative" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-warning positive" data-dismiss="modal">Ubah</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("input").attr("autocomplete","off");
        });
    </script>
</body>

</html>
