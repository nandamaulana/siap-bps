<header class="navbar navbar-expand-md navbar-light">
    <div class="container-xl">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a href="<?= BASE_URL ?>" class="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pr-0 pr-md-3">
            <img src="<?= BASE_URL ?>assets/img/logo.png" alt="Tabler" class="navbar-brand-image">
            <h3 class="my-auto ml-2" >Bidan Delima</h3>
        </a>
        <div class="navbar-nav flex-row order-md-last">
            <div class="nav-item dropdown">
                <a href="#" class="nav-link d-flex lh-1 text-reset p-0" data-toggle="dropdown">
                    <span class="avatar" style="background-image: url(<?= BASE_URL ?>assets/img/logo.png)"></span>
                    <div class="d-none d-xl-block pl-2">
                        <div><?= $_SESSION["nama"] ?></div>
                        <div class="mt-1 small text-muted"><?= $_SESSION["tipe_pengguna"] ?></div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">
                        <i class="fas fa-cog mr-2"></i>
                        Ganti Password
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?= BASE_URL ?>logout">
                        <i class="fas fa-sign-out-alt mr-2"></i>
                        Logout
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
