<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
        <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
        <title>Sign in - Tabler - Premium and Open Source dashboard template with responsive and high quality UI.</title>
        <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
        <meta name="msapplication-TileColor" content="#206bc4"/>
        <meta name="theme-color" content="#206bc4"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <meta name="mobile-web-app-capable" content="yes"/>
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="robots" content="noindex,nofollow,noarchive"/>
        <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon"/>
        <!-- CSS files -->
        <link href="<?=BASEPATH?>assets/css/tabler.min.css" rel="stylesheet"/>
        <link href="<?=BASEPATH?>assets/css/tabler-template.min.css" rel="stylesheet"/>
        <style>
        body {
            display: none;
        }
        </style>
    </head>
    <body class="antialiased border-top-wide border-primary d-flex flex-column">
        <div class="flex-fill d-flex flex-column justify-content-center">
            <div class="container-tight py-6">
                <div class="text-center mb-4">
                    <img src="assets/img/logo.png" height="128" alt="">
                </div>
                <form class="card card-md" action="login/verify" method="post">
                    <div class="card-body">
                        <h2 class="mb-5 text-center">Login Pengguna</h2>
                        <?php if (!empty($_SESSION["flash"])) { ?>
                            <div class="alert alert-danger" role="alert">
                                <?= $_SESSION["flash"] ?>
                            </div>
                        <?php } ?>
                        <div class="mb-3">
                            <label class="form-label">Alamat Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Masukkan Email" autocomplete="off">
                        </div>
                        <div class="mb-2">
                            <label class="form-label">
                                Password
                            </label>
                            <div class="input-group input-group-flat">
                                <input type="password" name="password" class="form-control"  placeholder="Masukkan Password" >
                            </div>
                        </div>
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Libs JS -->
        <script src="<?= BASEPATH ?>assets/plugins/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Tabler Core -->
        <script src="<?= BASEPATH ?>assets/js/tabler.min.js"></script>
        <script>
            document.body.style.display = "block"
        </script>
    </body>
</html>
