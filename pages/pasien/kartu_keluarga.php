<style>
    .btn-block {
        margin-top: unset !important;
        width: unset !important;
    }

    .btn .icon {
        margin: unset !important;
    }
</style>
<div class="content">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        <?= $title ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data <?= $title ?></h3>
            </div>
            <div class="card-body">
                <div class="alert_div"></div>
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-1 pull-left" style="text-align:left;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <input type="text" id="search-table" class="form-control bg-warning-lighter pull-right" placeholder="ketik disini . .">
                    </div>
                </div>
                <div class="row">
                    <table class="table table-hover" style="width:100%" id="tabel">
                        <thead>
                            <tr>
                                <th class="th-no" style="width:5%; text-align: center">No.</th>
                                <th class="th-no_kk">No. Kartu Keluarga</th>
                                <th class="th-alamat">Alamat</th>
                                <th class="th-aksi" style="width:20%; text-align: center">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade slide-down disable-scroll" id="modal_tambah" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <h5 class="modal-title">Data <?= $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="far fa-window-minimize"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="" id="form_tambah" action="javascript:;">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>No. Kartu Keluarga</label>
                                    <input id="no_kk" name="no_kk" placeholder="Masukkan No. Kartu Keluarga..." type="text" minlength="16" maxlength="16" class="form-control kode_desa" required>
                                    <input id="edit" name="edit" type="hidden" class="edit" >
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Alamat</label>
                                    <textarea id="alamat" name="alamat" placeholder="Isi Alamat..." rows="3" class="form-control alamat" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-primary ml-auto" id="btn_mod_simpan">
                    Simpan
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    var main;
    var page_script = {
        data_table: null,
        modal_tambah: null,
        modal_hapus: null,
        load_data: function() {
            main.block();
            var setting = main.settings_table_server_side;
            setting.ajax = {
                "url": main.baseUrl + "kartu_keluarga/load_data",
                "type": "POST"
            }
            setting.drawCallback = function(settings) {
                main.unblock();
            }
            setting.order = [
                [0, "asc"]
            ];
            setting.columnDefs = [{
                    targets: 'th-no',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.no + ".";
                    }
                },
                {
                    targets: 'th-no_kk',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.no_kk;
                    }
                },
                {
                    targets: 'th-alamat',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.alamat;
                    }
                },
                {
                    targets: 'th-aksi',
                    orderable: false,
                    className: "text-center",
                    render: function(data, type, row) {
                        return '<a href="javascript:;" onclick="page_script.edit(this)"\
                        data-no_kk="' + row.no_kk + '"\
                        data-alamat="' + row.alamat + '"\
                        class="btn btn-success mr-3"\
                        data-toggle="tooltip" data-placement="top" title="Edit"><i class="far fa-edit"></i></a>' +
                            '<a href="javascript:;" onclick="page_script.konfirmasi_hapus(this)"\
                        data-no_kk="' + row.no_kk + '"\
                        class="btn btn-danger"\
                        data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fas fa-trash"></i></a>';
                    }
                }
            ]
            page_script.data_table = $("#tabel").DataTable(setting);
        },
        konfirmasi_hapus: function(element) {
            var data = $(element).data();
            page_script.modal_hapus.find(".modal-title").html("Hapus data " + data.no_kk + " ?")
            page_script.modal_hapus.find(".positive").attr('onclick', "page_script.hapus('" + data.no_kk + "')")
            page_script.modal_hapus.modal('show');
        },
        simpan: function() {
            main.block()
            var data_send = {}
            data_send.edit = $("#edit").val()
            data_send.no_kk = $("#no_kk").val()
            data_send.alamat = $("#alamat").val()


            main.ajax("kartu_keluarga/simpan", JSON.stringify(data_send), function(msg) {
                var data = JSON.parse(msg);

                if (data.status_code != 200) {
                    page_script.notifikasi("warning", "Simpan Gagal");
                } else {
                    if (data.status_data == -1) {
                        main.notifikasi("warning", "No. Kartu Keluarga Sudah Ada");
                    } else {
                        main.notifikasi("success", "Data Tersimpan");
                    }
                    page_script.data_table.ajax.reload();
                    $("#modal_tambah").modal('hide');
                }
                main.unblock();
            });
        },
        hapus: function(id) {
            var data_send = {}
            data_send.id = id
            main.ajax("Kartu_keluarga/delete", JSON.stringify(data_send), function(msg) {
                var data = JSON.parse(msg);

                if (data.status_code != 200) {
                    page_script.notifikasi("danger", "Hapus Gagal");
                } else {
                    main.notifikasi("warning", "Hapus Berhasil");
                    page_script.data_table.ajax.reload();
                    page_script.modal_hapus.modal('hide');
                }
                main.unblock();
            });
        },
        edit: function(element) {
            var data = $(element).data();
            page_script.modal_tambah.find("#edit").val("1");
            page_script.modal_tambah.find("#no_kk").val(data.no_kk);
            page_script.modal_tambah.find("#alamat").val(data.alamat);
            page_script.modal_tambah.find("#no_kk").attr('readonly', true);
            page_script.modal_tambah.modal('show');
        },
        reset_form: function() {
            page_script.modal_tambah.find("#edit").val(null);
            page_script.modal_tambah.find("#no_kk").attr('readonly', false);
            page_script.modal_tambah.find("#no_kk").val(null);
            page_script.modal_tambah.find("#alamat").val(null);
        },
        init: function() {
            page_script.load_data();
            page_script.modal_tambah = $("#modal_tambah");
            page_script.modal_hapus = $("#modal_hapus");
            $('#search-table').keyup(function() {
                var cari = $(this).val();
                main.delay(function() {
                    $("#tabel").dataTable().fnFilter(cari);
                }, 500);
            });

            $("#tambah").on('click', function() {
                page_script.reset_form();
                page_script.modal_tambah.modal('show');
            });

            $("#btn_mod_simpan").click(function() {
                if ($('#form_tambah').valid())
                    page_script.simpan();
            });
        }
    };
    $(document).ready(function() {
        main = Object.create(main_js);
        main.init("<?= BASE_URL ?>");
        page_script.init();
    });
</script>
