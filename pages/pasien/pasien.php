<style>
    .btn-block {
        margin-top: unset !important;
        width: unset !important;
    }

    .btn .icon {
        margin: unset !important;
    }
</style>
<div class="content">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        <?= $title ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data <?= $title ?></h3>
                <div class="d-flex flex-row ml-auto">
                    <a href="#" class="btn btn-primary btn-block mr-3" id="tambah">Tambah Data</a>
                </div>
            </div>
            <div class="card-body">
                <div class="alert_div"></div>
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-1 pull-left" style="text-align:left;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <input type="text" id="search-table" class="form-control bg-warning-lighter pull-right" placeholder="ketik disini . .">
                    </div>
                </div>
                <div class="row">
                    <!-- <table class="table table-hover" style="width:100%" id="tabel">
                        <thead>
                            <tr>
                                <th class="th-no" style="width:5%; text-align: center">No.</th>
                                <th class="th-kode_desa">Kode Desa</th>
                                <th class="th-nama_desa">Nama Desa</th>
                                <th class="th-aksi" style="width:20%; text-align: center">Aksi</th>
                            </tr>
                        </thead>
                    </table> -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade slide-down disable-scroll" id="modal_tambah" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <h5 class="modal-title">Data <?= $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="far fa-window-minimize"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="" id="form_tambah" action="javascript:;">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Kode Desa</label>
                                    <input id="kode_desa" name="kode_desa" placeholder="Auto Generate" type="text" class="form-control kode_desa" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Nama Desa</label>
                                    <input id="nama_desa" name="nama_desa" placeholder="Isi Nama Desa..." type="text" class="form-control nama_desa">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-primary ml-auto" id="btn_mod_simpan">
                    Simpan
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    var main;
    var page_script = {
        data_table: null,
        modal_tambah: null,
        modal_hapus: null,
        load_data: function() {
            main.block();
            var setting = main.settings_table_server_side;
            setting.ajax = {
                "url": main.baseUrl + "master_desa/load_data",
                "type": "POST"
            }
            setting.drawCallback = function(settings) {
                main.unblock();
            }
            setting.order = [
                [0, "asc"]
            ];
            setting.columnDefs = [{
                    targets: 'th-no',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.no + ".";
                    }
                },
                {
                    targets: 'th-kode_desa',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.kode_desa;
                    }
                },
                {
                    targets: 'th-nama_desa',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_desa;
                    }
                },
                {
                    targets: 'th-aksi',
                    orderable: false,
                    className: "text-center",
                    render: function(data, type, row) {
                        return '<a href="javascript:;" onclick="page_script.edit(this)"\
                        data-kode_desa="' + row.kode_desa + '"\
                        data-nama_desa="' + row.nama_desa + '"\
                        class="btn btn-success mr-3"\
                        data-toggle="tooltip" data-placement="top" title="Edit"><i class="far fa-edit"></i></a>' +
                            '<a href="javascript:;" onclick="page_script.konfirmasi_hapus(this)"\
                        data-kode_desa="' + row.kode_desa + '"\
                        class="btn btn-danger"\
                        data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fas fa-trash"></i></a>';
                    }
                }
            ]
            page_script.data_table = $("#tabel").DataTable(setting);
        },
        konfirmasi_hapus: function(element) {
            var data = $(element).data();
            page_script.modal_hapus.find(".modal-title").html("Hapus data " + data.kode_desa + " ?")
            page_script.modal_hapus.find(".positive").attr('onclick', "page_script.hapus('" + data.kode_desa + "')")
            page_script.modal_hapus.modal('show');
        },
        simpan: function() {
            main.block()
            var data_send = {}
            data_send.kode_desa = $("#kode_desa").val()
            data_send.nama_desa = $("#nama_desa").val()

            ssi.ajax("master_desa/simpan",JSON.stringify(data_send), function(msg){
                var data = JSON.parse(msg);

                if(data.status_code != 200){
                    page_script.display_error(data, 1);
                    ssi.notifikasi_simpan("0");
                }else{
                    if (data.status_data == 2) {
                        page_script.display_error({status_message:'Nama Label Sudah Ada', status_code:'200'}, 1);
                        ssi.notifikasi("Nama Label Sudah Ada",ssi.default_timer,"warning");
                        return;
                    }
                    ssi.notifikasi_simpan(data.status_data);
                    page_script.datatable_1.ajax.reload();
                    $("#modal_tambah").modal('hide');
                }
                ssi.unblok();
            });
        },
        hapus: function(id) {

        },
        edit: function(element) {
            var data = $(element).data();
            page_script.modal_tambah.find("#kode_desa").val(data.kode_desa);
            page_script.modal_tambah.find("#nama_desa").val(data.nama_desa);
            page_script.modal_tambah.modal('show');
        },
        reset_form: function() {
            page_script.modal_tambah.find("#kode_desa").val(null);
            page_script.modal_tambah.find("#nama_desa").val(null);
        },
        init: function() {
            // page_script.load_data();
            // page_script.modal_tambah = $("#modal_tambah");
            // page_script.modal_hapus = $("#modal_hapus");
            $('#search-table').keyup(function() {
                var cari = $(this).val();
                main.delay(function() {
                    $("#tabel").dataTable().fnFilter(cari);
                }, 500);
            });

            $("#tambah").on('click', function() {
                page_script.reset_form();
                page_script.modal_tambah.modal('show');
            });
        }
    };
    $(document).ready(function() {
        main = Object.create(main_js);
        main.init("<?= BASE_URL ?>");
        page_script.init();
    });
</script>
