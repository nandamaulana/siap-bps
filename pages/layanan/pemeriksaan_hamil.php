<style>
    .btn-block {
        margin-top: unset !important;
        width: unset !important;
    }

    .btn .icon {
        margin: unset !important;
    }
</style>
<div class="content">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        <?= $title ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data <?= $title ?></h3>
                <div class="d-flex flex-row ml-auto">
                    <a href="#" class="btn btn-primary btn-block mr-3" id="tambah">Tambah Data</a>
                </div>
            </div>
            <div class="card-body">
                <div class="alert_div"></div>
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-1 pull-left" style="text-align:left;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <input type="text" id="search-table" class="form-control bg-warning-lighter pull-right" placeholder="ketik disini . .">
                    </div>
                </div>
                <div class="row">
                    <table class="table table-hover" style="width:100%" id="tabel">
                        <thead>
                            <tr>
                                <th class="th-no" style="width:5%; text-align: center">No.</th>
                                <th class="th-kode_desa">Kode Desa</th>
                                <th class="th-nama_desa">Nama Desa</th>
                                <th class="th-aksi" style="width:20%; text-align: center">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade slide-down disable-scroll" id="modal_tambah" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <h5 class="modal-title">Data <?= $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="far fa-window-minimize"></i>
                </button>
            </div>
            <ul class="nav nav-tabs nav-fill" data-toggle="tabs">
                <li class="nav-item">
                    <a href="#tabs-home-ex5" class="nav-link active" data-toggle="tab">
                        Pasien Lama
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#tabs-pasien-baru" class="nav-link" data-toggle="tab">
                        Pasien Baru
                    </a>
                </li>
            </ul>
            <div class="modal-body">
                <div class="tab-content">
                    <div class="tab-pane active show" id="tabs-home-ex5">
                        <form class="" id="form_pasien_lama" action="javascript:;">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Kode Desa</label>
                                        <div class="form-group form-group-default input-group">
                                            <input id="kode_desa" name="kode_desa" placeholder="Auto Generate" type="text" class="form-control kode_desa" readonly>
                                            <span class="input-group-text">
                                                <a href="javascript:;" onclick="page_script.change_mode(this)" class="input-group-link mode">Manual</a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-default">
                                            <label>Nama Desa</label>
                                            <input id="nama_desa" name="nama_desa" placeholder="Isi Nama Desa..." type="text" class="form-control nama_desa" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="tabs-pasien-baru">
                        <form class="" id="form_pasien_baru" action="javascript:;">
                            <div class="form-group">
                                <h4 class="bold">Kartu Keluarga</h4>
                                <div class="row">
                                    <div class="form-group form-group-default input-group">
                                        <label class="form-check">
                                            <input id="sudah_punya_kk" name="sudah_punya_kk" class="form-check-input sudah_punya_kk" type="checkbox" checked="">
                                            <span class="form-check-label">Sudah Punya KK</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row sudah_punya_kk_field">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                            <label>KK Terdaftar</label>
                                            <select id="kk_terdaftar" name="kk_terdaftar" placeholder="Pilih KK..." class="form-control kk_terdaftar">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row belum_punya_kk_field" style="display:none;">
                                    <div class="col-md-6">
                                        <label>No.kk</label>
                                        <div class="form-group form-group-default input-group">
                                            <input id="no_kk_baru" name="no_kk_baru" placeholder="No KK..." type="text" class="form-control no_kk_baru">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Alamat</label>
                                        <div class="form-group form-group-default input-group">
                                            <input id="alamat_kk_baru" name="alamat_kk_baru" placeholder="No KK..." type="text" class="form-control alamat_kk_baru">
                                        </div>
                                    </div>
                                </div>
                                <h4 class="bold">Data Pasien</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Nama Pasien</label>
                                        <div class="form-group form-group-default input-group">
                                            <input id="nama_pasien_baru" name="nama_pasien_baru" placeholder="Nama Pasien" type="text" class="form-control nama_pasien_baru" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-default">
                                            <label>Pilih Desa</label>
                                            <select id="desa_pasien_baru" name="desa_pasien_baru" placeholder="Pilih Tanggal Lahir..." class="form-control desa_pasien_baru">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-default">
                                            <label>Tanggal Lahir</label>
                                            <input id="tanggal_lahir_pasien_baru" name="tanggal_lahir_pasien_baru" placeholder="Pilih Tanggal Mulai..." type="text" class="form-control tanggal_lahir_pasien_baru" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-default">
                                            <label>Jenis Kelamin</label>
                                            <select id="jenis_kelamin" name="jenis_kelamin" placeholder="Pilih Jenis Kelamin..." class="form-control jenis_kelamin">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-default">
                                            <label>Status Hubungan</label>
                                            <select id="status_hubungan_pasien_baru" name="status_hubungan_pasien_baru" placeholder="Pilih Jenis Kelamin..." class="form-control status_hubungan_pasien_baru">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-default">
                                            <label>No. Telp Pasien</label>
                                            <input id="no_telp_pasien_baru" name="no_telp_pasien_baru" placeholder="Pilih Tanggal Mulai..." type="text" class="form-control no_telp_pasien_baru" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-primary ml-auto" id="btn_mod_simpan">
                    Simpan
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    var main;
    var page_script = {
        data_table: null,
        modal_tambah: null,
        modal_hapus: null,
        load_data: function() {
            main.block();
            var setting = main.settings_table_server_side;
            setting.ajax = {
                "url": main.baseUrl + "master_desa/load_data",
                "type": "POST"
            }
            setting.drawCallback = function(settings) {
                main.unblock();
            }
            setting.order = [
                [0, "asc"]
            ];
            setting.columnDefs = [{
                    targets: 'th-no',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.no + ".";
                    }
                },
                {
                    targets: 'th-kode_desa',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.kode_desa;
                    }
                },
                {
                    targets: 'th-nama_desa',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_desa;
                    }
                },
                {
                    targets: 'th-aksi',
                    orderable: false,
                    className: "text-center",
                    render: function(data, type, row) {
                        console.log(row);
                        return '<a href="javascript:;" onclick="page_script.edit(this)"\
                        data-kode_desa="' + row.kode_desa + '"\
                        data-nama_desa="' + row.nama_desa + '"\
                        class="btn btn-success mr-3"\
                        data-toggle="tooltip" data-placement="top" title="Edit"><i class="far fa-edit"></i></a>' +
                        '<a href="javascript:;" onclick="page_script.konfirmasi_hapus(this)"\
                        data-kode_desa="' + row.kode_desa + '"\
                        data-deleted="' + row.deleted + '"\
                        class="btn btn-'+ (row.deleted == 1 ? "success" : "danger") +'"\
                        data-toggle="tooltip" data-placement="top" title="'+ (row.deleted == 1 ? "non aktifkan" : "aktifkan") +'">\
                            <i class="'+ (row.deleted == 1 ? "fas fa-check" : "fas fa-ban") +'"></i>\
                        </a>';
                    }
                }
            ]
            page_script.data_table = $("#tabel").DataTable(setting);
        },
        konfirmasi_hapus: function(element) {
            var data = $(element).data();
            page_script.modal_hapus.find(".modal-title").html("Update Status data " + data.kode_desa + " ?")
            page_script.modal_hapus.find(".positive").attr('onclick', "page_script.hapus('" + data.kode_desa + "'," + data.deleted + ")")
            page_script.modal_hapus.modal('show');
        },
        simpan: function() {
            main.block()
            var data_send = {}
            data_send.manual = !$("#kode_desa").prop('readonly')
            data_send.kode_desa = $("#kode_desa").val()
            data_send.nama_desa = $("#nama_desa").val()

            main.ajax("master_desa/simpan",JSON.stringify(data_send), function(msg){
                var data = JSON.parse(msg);

                if(data.status_code != 200){
                    page_script.notifikasi("warning", "Simpan Gagal");
                }else{
                    if (data.status_data == -1) {
                        main.notifikasi("warning","Nama Desa Sudah Ada");
                    }else{
                        main.notifikasi("success", "Data Tersimpan");
                    }
                    page_script.data_table.ajax.reload();
                    $("#modal_tambah").modal('hide');
                }
                main.unblock();
            });
        },
        hapus: function(id,deleted) {
            var data_send = {}
            data_send.id = id;
            data_send.deleted = deleted;
            main.ajax("master_desa/delete",JSON.stringify(data_send), function(msg){
                var data = JSON.parse(msg);

                if(data.status_code != 200){
                    page_script.notifikasi("danger", "Ubah Gagal");
                }else{
                    main.notifikasi("warning", "Ubah Berhasil");
                    page_script.data_table.ajax.reload();
                    page_script.modal_hapus.modal('hide');
                }
                main.unblock();
            });
        },
        edit: function(element) {
            var data = $(element).data();
            page_script.reset_form();
            page_script.modal_tambah.find("#kode_desa").val(data.kode_desa);
            page_script.modal_tambah.find("#nama_desa").val(data.nama_desa);
            page_script.modal_tambah.modal('show');
        },
        reset_form: function() {
            if($(".mode").html() == "Auto"){
                page_script.change_mode($(".mode"));
            }
            page_script.modal_tambah.find("#kode_desa").val(null);
            page_script.modal_tambah.find("#nama_desa").val(null);
        },
        change_mode: function(element){
            $(element).html($(element).html() == "Manual" ? "Auto" : "Manual");
            $("#kode_desa").val(null);
            $("#kode_desa").attr('readonly',$(element).html() == "Manual");
            $("#kode_desa").attr('required',$(element).html() != "Manual");
            $("#kode_desa").attr('placeholder',$(element).html() == "Manual" ? "Auto Generate" : "Isi Kode");
        },
        init: function() {

            $("#sudah_punya_kk").on('click', function(event){
                if($(this)[0].checked){
                    $(".sudah_punya_kk_field").show('fast')
                    $(".belum_punya_kk_field").hide('fast')
                    return;
                }
                $(".sudah_punya_kk_field").hide('fast')
                $(".belum_punya_kk_field").show('fast')
            });

            // page_script.load_data();
            page_script.modal_tambah = $("#modal_tambah");
            page_script.modal_hapus = $("#modal_hapus");
            // $('#search-table').keyup(function() {
            //     var cari = $(this).val();
            //     main.delay(function() {
            //         $("#tabel").dataTable().fnFilter(cari);
            //     }, 500);
            // });

            $("#tambah").on('click', function() {
                page_script.reset_form();
                page_script.modal_tambah.modal('show');
            });

            $("#btn_mod_simpan").click(function(){
                if($('#form_tambah').valid())
                    page_script.simpan();
            });
        }
    };
    $(document).ready(function() {
        main = Object.create(main_js);
        main.init("<?= BASE_URL ?>");
        page_script.init();
    });
</script>
