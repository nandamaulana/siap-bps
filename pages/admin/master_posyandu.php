<style>
    .btn-block {
        margin-top: unset !important;
        width: unset !important;
    }

    .btn .icon {
        margin: unset !important;
    }
</style>
<div class="content">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        <?= $title ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data <?= $title ?></h3>
                <div class="d-flex flex-row ml-auto">
                    <a href="#" class="btn btn-primary btn-block mr-3" id="tambah">Tambah Data</a>
                </div>
            </div>
            <div class="card-body">
                <div class="alert_div"></div>
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-1 pull-left" style="text-align:left;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <input type="text" id="search-table" class="form-control bg-warning-lighter pull-right" placeholder="ketik disini . .">
                    </div>
                </div>
                <div class="row">
                    <table class="table table-hover" style="width:100%" id="tabel">
                        <thead>
                            <tr>
                                <th class="th-no" style="width:5%; text-align: center">No.</th>
                                <th class="th-id_posyandu">ID Posyandu</th>
                                <th class="th-nama_desa">Nama Desa</th>
                                <th class="th-nama_posyandu">Nama Posyandu</th>
                                <th class="th-telp_posyandu">Telp.</th>
                                <th class="th-alamat_posyandu">Alamat</th>
                                <th class="th-aksi" style="width:20%; text-align: center">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade slide-down disable-scroll" id="modal_tambah" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <h5 class="modal-title">Data <?= $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="far fa-window-minimize"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="" id="form_tambah" action="javascript:;">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group form-group-default">
                                    <label>ID Posyandu</label>
                                    <input id="id_posyandu" name="id_posyandu" placeholder="Auto Generate" type="text" class="form-control id_posyandu" readonly>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-default">
                                    <label>Desa</label>
                                    <select id="id_desa" name="id_desa" placeholder="Pilih Desa..." class="form-control id_desa">
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-default">
                                    <label>Nama Posyandu</label>
                                    <input id="nama_posyandu" name="nama_posyandu" placeholder="Isi Nama Posyandu..." type="text" class="form-control nama_posyandu">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-default">
                                    <label>Telp</label>
                                    <input id="telp_posyandu" name="telp_posyandu" placeholder="Isi Telp..." type="text" class="form-control telp_posyandu">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group form-group-default">
                                    <label>Alamat</label>
                                    <textarea id="alamat_posyandu" name="alamat_posyandu" placeholder="Isi Alamat Posyandu..." type="text" class="form-control alamat_posyandu" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-primary ml-auto" id="btn_mod_simpan">
                    Simpan
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    var main;
    var page_script = {
        data_table: null,
        modal_tambah: null,
        modal_hapus: null,
        selectized_desa: null,
        load_data: function() {
            main.block();
            var setting = main.settings_table_server_side;
            setting.ajax = {
                "url": main.baseUrl + "master_posyandu/load_data",
                "type": "POST"
            }
            setting.drawCallback = function(settings) {
                main.unblock();
            }
            setting.order = [
                [0, "asc"]
            ];
            setting.columnDefs = [{
                    targets: 'th-no',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.no + ".";
                    }
                },
                {
                    targets: 'th-id_posyandu',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.id_posyandu;
                    }
                },
                {
                    targets: 'th-nama_desa',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_desa;
                    }
                },
                {
                    targets: 'th-nama_posyandu',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_posyandu;
                    }
                },
                {
                    targets: 'th-telp_posyandu',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.telp_posyandu;
                    }
                },
                {
                    targets: 'th-alamat_posyandu',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.alamat_posyandu;
                    }
                },
                {
                    targets: 'th-aksi',
                    orderable: false,
                    className: "text-center",
                    render: function(data, type, row) {
                        return '<a href="javascript:;" onclick="page_script.edit(this)"\
                        data-id_posyandu="' + row.id_posyandu + '"\
                        data-id_desa="' + row.id_desa + '"\
                        data-nama_posyandu="' + row.nama_posyandu + '"\
                        data-alamat_posyandu="' + row.alamat_posyandu + '"\
                        data-telp_posyandu="' + row.telp_posyandu + '"\
                        class="btn btn-success mr-3"\
                        data-toggle="tooltip" data-placement="top" title="Edit"><i class="far fa-edit"></i></a>' +
                        '<a href="javascript:;" onclick="page_script.konfirmasi_hapus(this)"\
                        data-deleted="' + row.deleted + '"\
                        data-id_posyandu="' + row.id_posyandu + '"\
                        class="btn btn-'+ (row.deleted == 1 ? "success" : "danger") +'"\
                        data-toggle="tooltip" data-placement="top" title="'+ (row.deleted == 1 ? "non aktifkan" : "aktifkan") +'">\
                            <i class="'+ (row.deleted == 1 ? "fas fa-check" : "fas fa-ban") +'"></i>\
                        </a>';
                    }
                }
            ]
            page_script.data_table = $("#tabel").DataTable(setting);
        },
        konfirmasi_hapus: function(element) {
            var data = $(element).data();
            page_script.modal_hapus.find(".modal-title").html("Ubah status data " + data.id_posyandu + " ?")
            page_script.modal_hapus.find(".positive").attr('onclick', "page_script.hapus('" + data.id_posyandu + "',"+data.deleted+")")
            page_script.modal_hapus.modal('show');
        },
        simpan: function() {
            main.block()
            var data_send = {}
            data_send.id_posyandu = $("#id_posyandu").val()
            data_send.id_desa = $("#id_desa").val()
            data_send.nama_posyandu = $("#nama_posyandu").val()
            data_send.telp_posyandu = $("#telp_posyandu").val()
            data_send.alamat_posyandu = $("#alamat_posyandu").val()

            main.ajax("master_posyandu/simpan", JSON.stringify(data_send), function(msg) {
                var data = JSON.parse(msg);

                if (data.status_code != 200) {
                    page_script.notifikasi("warning", "Simpan Gagal");
                } else {
                    if (data.status_data == -1) {
                        main.notifikasi("warning", "Nama Posyandu Sudah Ada");
                    } else {
                        main.notifikasi("success", "Data Tersimpan");
                    }
                    page_script.data_table.ajax.reload();
                    $("#modal_tambah").modal('hide');
                }
            });
        },
        hapus: function(id, deleted) {
            var data_send = {}
            data_send.id = id
            data_send.deleted = deleted
            main.ajax("master_posyandu/delete", JSON.stringify(data_send), function(msg) {
                var data = JSON.parse(msg);

                if (data.status_code != 200) {
                    page_script.notifikasi("danger", "Ubah Gagal");
                } else {
                    main.notifikasi("warning", "Ubah Berhasil");
                    page_script.data_table.ajax.reload();
                    page_script.modal_hapus.modal('hide');
                }
                main.unblock();
            });
        },
        edit: function(element) {
            var data = $(element).data();
            page_script.selectized_desa.addOption({
                id: data.id_desa,
                field: data.nama_desa
            })
            page_script.selectized_desa.setValue(data.id_desa)
            page_script.modal_tambah.find("#id_posyandu").val(data.id_posyandu);
            page_script.modal_tambah.find("#id_desa").val(data.id_desa);
            page_script.modal_tambah.find("#nama_posyandu").val(data.nama_posyandu);
            page_script.modal_tambah.find("#alamat_posyandu").val(data.alamat_posyandu);
            page_script.modal_tambah.find("#telp_posyandu").val(data.telp_posyandu);
            page_script.modal_tambah.modal('show');
        },
        reset_form: function() {
            page_script.modal_tambah.find("#id_posyandu").val(null);
            page_script.modal_tambah.find("#id_desa").val(null);
            page_script.modal_tambah.find("#nama_posyandu").val(null);
            page_script.clear_select_desa();
            page_script.modal_tambah.find("#telp_posyandu").val(null);
            page_script.modal_tambah.find("#alamat_posyandu").val(null);
        },
        clear_select_desa: function() {
            page_script.selectized_desa.clearOptions()
        },
        init: function() {
            page_script.load_data();
            page_script.modal_tambah = $("#modal_tambah");
            page_script.modal_hapus = $("#modal_hapus");
            $('#search-table').keyup(function() {
                var cari = $(this).val();
                main.delay(function() {
                    $("#tabel").dataTable().fnFilter(cari);
                }, 500);
            });

            $("#tambah").on('click', function() {
                page_script.reset_form();
                page_script.modal_tambah.modal('show');
            });

            $("#btn_mod_simpan").click(function() {
                if ($('#form_tambah').valid())
                    page_script.simpan();
            });

            page_script.selectized_desa = main.selectize_remote("#id_desa", "master_desa/get_combobox/")[0].selectize

            page_script.modal_tambah.on('keyup', '#id_desa-selectized', function() {
                if ($(this).val().length == 0) {
                    page_script.clear_select_desa();
                    $(this).blur()
                    $(this).focus()
                }
            })
            page_script.modal_tambah.on('shown.bs.modal', function() {
                page_script.clear_select_desa()
                page_script.selectized_desa.blur()
            })
        }
    };
    $(document).ready(function() {
        main = Object.create(main_js);
        main.init("<?= BASE_URL ?>");
        page_script.init();
    });
</script>
