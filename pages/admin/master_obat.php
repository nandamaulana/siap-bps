<style>
    .btn-block {
        margin-top: unset !important;
        width: unset !important;
    }

    .btn .icon {
        margin: unset !important;
    }
</style>
<div class="content">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        <?= $title ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data <?= $title ?></h3>
                <div class="d-flex flex-row ml-auto">
                    <a href="#" class="btn btn-primary btn-block mr-3" id="tambah">Tambah Data</a>
                </div>
            </div>
            <div class="card-body">
                <div class="alert_div"></div>
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-1 pull-left" style="text-align:left;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <input type="text" id="search-table" class="form-control bg-warning-lighter pull-right" placeholder="ketik disini . .">
                    </div>
                </div>
                <div class="row">
                    <table class="table table-hover" style="width:100%" id="tabel">
                        <thead>
                            <tr>
                                <th class="th-no" style="width:5%; text-align: center">No.</th>
                                <th class="th-id_obat">ID Obat</th>
                                <th class="th-nama_obat">Nama Obat</th>
                                <th class="th-nama_bentuk">Nama Bentuk</th>
                                <th class="th-nama_kategori">Nama Kategori</th>
                                <th class="th-dosis">Dosis</th>
                                <th class="th-aksi" style="width:20%; text-align: center">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade slide-down disable-scroll" id="modal_tambah" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <h5 class="modal-title">Data <?= $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="far fa-window-minimize"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="" id="form_tambah" action="javascript:;">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>ID Obat</label>
                                    <input id="id_obat" name="id_obat" placeholder="Auto Generate" type="text" class="form-control id_obat" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Nama Obat</label>
                                    <input id="nama_obat" name="nama_obat" placeholder="Isi Nama Obat..." type="text" class="form-control nama_obat">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>Nama Bentuk</label>
                                    <select id="id_bentuk" name="id_bentuk" placeholder="Pilih Bentuk..." class="form-control id_bentuk">
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>Nama Kategori</label>
                                    <select id="id_kategori" name="id_kategori" placeholder="Pilih Kategori..." class="form-control id_kategori">
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>Dosis</label>
                                    <input id="dosis" name="dosis" placeholder="Isi Dosis..." type="text" class="form-control dosis">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-primary ml-auto" id="btn_mod_simpan">
                    Simpan
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    var main;
    var page_script = {
        data_table: null,
        modal_tambah: null,
        modal_hapus: null,
        selectized_bentuk: null,
        selectized_kategori: null,
        load_data: function() {
            main.block();
            var setting = main.settings_table_server_side;
            setting.ajax = {
                "url": main.baseUrl + "master_obat/load_data",
                "type": "POST"
            }
            setting.drawCallback = function(settings) {
                main.unblock();
            }
            setting.order = [
                [0, "asc"]
            ];
            setting.columnDefs = [{
                    targets: 'th-no',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.no + ".";
                    }
                },
                {
                    targets: 'th-id_obat',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.id_obat;
                    }
                },
                {
                    targets: 'th-nama_bentuk',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_bentuk;
                    }
                },
                {
                    targets: 'th-nama_kategori',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_kategori;
                    }
                },
                {
                    targets: 'th-nama_obat',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_obat;
                    }
                },
                {
                    targets: 'th-dosis',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.dosis;
                    }
                },
                {
                    targets: 'th-aksi',
                    orderable: false,
                    className: "text-center",
                    render: function(data, type, row) {
                        return '<a href="javascript:;" onclick="page_script.edit(this)"\
                        data-id_obat="' + row.id_obat + '"\
                        data-id_bentuk="' + row.id_bentuk + '"\
                        data-nama_bentuk="' + row.nama_bentuk + '"\
                        data-id_kategori="' + row.id_kategori + '"\
                        data-nama_kategori="' + row.nama_kategori + '"\
                        data-nama_obat="' + row.nama_obat + '"\
                        data-dosis="' + row.dosis + '"\
                        class="btn btn-success mr-3"\
                        data-toggle="tooltip" data-placement="top" title="Edit"><i class="far fa-edit"></i></a>' +
                            '<a href="javascript:;" onclick="page_script.konfirmasi_hapus(this)"\
                        data-id_obat="' + row.id_obat + '"\
                        data-deleted="' + row.deleted_status + '"\
                        class="btn btn-'+ (row.deleted_status == 1 ? "success" : "danger") +'"\
                        data-toggle="tooltip" data-placement="top" title="'+ (row.deleted_status == 1 ? "non aktifkan" : "aktifkan") +'">\
                            <i class="'+ (row.deleted_status == 1 ? "fas fa-check" : "fas fa-ban") +'"></i>\
                        </a>';
                    }
                }
            ]
            page_script.data_table = $("#tabel").DataTable(setting);
        },
        konfirmasi_hapus: function(element) {
            var data = $(element).data();
            page_script.modal_hapus.find(".modal-title").html("Ubah status data " + data.id_obat + " ?")
            page_script.modal_hapus.find(".positive").attr('onclick', "page_script.hapus('" + data.id_obat + "', "+data.deleted+")")
            page_script.modal_hapus.modal('show');
        },
        simpan: function() {
            main.block()
            var data_send = {}
            data_send.id_obat = $("#id_obat").val()
            data_send.id_bentuk = $("#id_bentuk").val()
            data_send.id_kategori = $("#id_kategori").val()
            data_send.nama_obat = $("#nama_obat").val()
            data_send.dosis = $("#dosis").val()

            main.ajax("master_obat/simpan",JSON.stringify(data_send), function(msg){
                var data = JSON.parse(msg);

                if(data.status_code != 200){
                    page_script.notifikasi("warning", "Simpan Gagal");
                }else{
                    if (data.status_data == -1) {
                        main.notifikasi("warning","Nama Obat Sudah Ada");
                    }else{
                        main.notifikasi("success", "Data Tersimpan");
                    }
                    page_script.data_table.ajax.reload();
                    $("#modal_tambah").modal('hide');
                }
                main.unblock();
            });
        },
        hapus: function(id,deleted) {
            var data_send = {}
            data_send.id = id
            data_send.deleted = deleted
            main.ajax("master_obat/delete", JSON.stringify(data_send), function(msg) {
                var data = JSON.parse(msg);
                if (data.status_code != 200) {
                    page_script.notifikasi("danger", "Ubah Gagal");
                } else {
                    main.notifikasi("warning", "Ubah Berhasil");
                    page_script.data_table.ajax.reload();
                    page_script.modal_hapus.modal('hide');
                }
                main.unblock();
            });
        },
        edit: async function(element) {
            var data = $(element).data();
            page_script.modal_tambah.find("#id_obat").val(data.id_obat);
            page_script.modal_tambah.find("#nama_obat").val(data.nama_obat);
            page_script.selectized_bentuk.addOption({
                id: data.id_bentuk,
                field: data.nama_bentuk
            })
            page_script.selectized_bentuk.setValue(data.id_bentuk)
            page_script.selectized_kategori.addOption({
                id: data.id_kategori,
                field: data.nama_kategori
            })
            page_script.selectized_kategori.setValue(data.id_kategori)
            page_script.modal_tambah.find("#dosis").val(data.dosis)
            page_script.modal_tambah.modal('show');
        },
        reset_form: function() {
            page_script.modal_tambah.find("#id_obat").val(null);
            page_script.modal_tambah.find("#nama_obat").val(null);
            page_script.clear_select_bentuk()
            page_script.clear_select_kategori()
            page_script.modal_tambah.find("#dosis").val(null)
        },
        clear_select_bentuk: function() {
            page_script.selectized_bentuk.clearOptions()
        },
        clear_select_kategori: async function() {
            page_script.selectized_kategori.clearOptions()
        },
        init: function() {
            page_script.load_data();
            page_script.modal_tambah = $("#modal_tambah");
            page_script.modal_hapus = $("#modal_hapus");
            $('#search-table').keyup(function() {
                var cari = $(this).val();
                main.delay(function() {
                    $("#tabel").dataTable().fnFilter(cari);
                }, 500);
            });

            $("#tambah").on('click', function() {
                page_script.reset_form();
                page_script.modal_tambah.modal('show');
            });

            page_script.selectized_bentuk = main.selectize_remote("#id_bentuk", "master_bentuk/get_combobox/")[0].selectize
            page_script.selectized_kategori = main.selectize_remote("#id_kategori", "master_kategori/get_combobox/")[0].selectize

            page_script.modal_tambah.on('keyup', '#id_bentuk-selectized', function() {
                if ($(this).val().length == 0) {
                    page_script.clear_select_bentuk()
                    $(this).blur()
                    $(this).focus()
                }
            })

            page_script.modal_tambah.on('keyup', '#id_kategori-selectized', function() {
                if ($(this).val().length == 0) {
                    page_script.clear_select_kategori()
                    $(this).blur()
                    $(this).focus()
                }
            })
            page_script.modal_tambah.on('shown.bs.modal', function() {
                page_script.clear_select_bentuk()
                page_script.clear_select_kategori()
                page_script.selectized_bentuk.blur()
                page_script.selectized_kategori.blur()
            })

            $("#btn_mod_simpan").click(function() {
                if ($('#form_tambah').valid())
                    page_script.simpan();
            });
        }
    };
    $(document).ready(function() {
        main = Object.create(main_js);
        main.init("<?= BASE_URL ?>");
        page_script.init();
    });
</script>
