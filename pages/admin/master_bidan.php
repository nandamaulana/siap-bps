<style>
    .btn-block {
        margin-top: unset !important;
        width: unset !important;
    }

    .btn .icon {
        margin: unset !important;
    }
</style>
<div class="content">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        <?= $title ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data <?= $title ?></h3>
                <div class="d-flex flex-row ml-auto">
                    <a href="#" class="btn btn-primary btn-block mr-3" id="tambah">Tambah Data</a>
                </div>
            </div>
            <div class="card-body">
                <div class="alert_div"></div>
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-1 pull-left" style="text-align:left;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <input type="text" id="search-table" class="form-control bg-warning-lighter pull-right" placeholder="ketik disini . .">
                    </div>
                </div>
                <div class="row">
                    <table class="table table-hover" style="width:100%" id="tabel">
                        <thead>
                            <tr>
                                <th class="th-no" style="width:5%; text-align: center">No.</th>
                                <th class="th-nip_bidan">NIP Bidan</th>
                                <th class="th-nama_bidan">Nama Bidan</th>
                                <th class="th-nama_desa">Nama Desa</th>
                                <th class="th-nama_posyandu">Nama Posyandu</th>
                                <th class="th-telp_bidan">Telp.</th>
                                <th class="th-aksi" style="width:20%; text-align: center">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade slide-down disable-scroll" id="modal_tambah" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <h5 class="modal-title">Data <?= $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="far fa-window-minimize"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="" id="form_tambah" action="javascript:;">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>NIP Bidan</label>
                                    <input id="nip_bidan" name="nip_bidan" placeholder="Isi Nip Bidan..." type="text" class="form-control nip_bidan" minlength="16" maxlength="16" required>
                                    <input id="edit" name="edit" placeholder="Isi Nip Bidan..." type="hidden" class="form-control edit" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Posyandu</label>
                                    <select id="id_posyandu" name="id_posyandu" placeholder="Pilih Posyandu..." class="form-control id_posyandu">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Nama Bidan</label>
                                    <input id="nama_bidan" name="nama_bidan" placeholder="Isi Nama Bidan..." type="text" class="form-control nama_bidan" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Telp.</label>
                                    <input id="telp_bidan" name="telp_bidan" placeholder="Isi Telp..." type="text" class="form-control telp_bidan">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Tanggal Mulai</label>
                                    <input id="tanggal_mulai" name="tanggal_mulai" placeholder="Pilih Tanggal Mulai..." type="text" class="form-control tanggal_mulai" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Tanggal Mulai Tugas</label>
                                    <input id="tanggal_mulai_tugas" name="tanggal_mulai_tugas" placeholder="Pilih Tanggal Mulai Tugas..." type="text" class="form-control tanggal_mulai_tugas">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-primary ml-auto" id="btn_mod_simpan">
                    Simpan
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    var main;
    var page_script = {
        data_table: null,
        modal_tambah: null,
        modal_hapus: null,
        date_tanggal_mulai: null,
        date_tanggal_mulai_tugas: null,
        selectized_posyandu: null,
        load_data: function() {
            main.block();
            var setting = main.settings_table_server_side;
            setting.ajax = {
                "url": main.baseUrl + "master_bidan/load_data",
                "type": "POST"
            }
            setting.drawCallback = function(settings) {
                main.unblock();
            }
            setting.order = [
                [0, "asc"]
            ];
            setting.columnDefs = [{
                    targets: 'th-no',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.no + ".";
                    }
                },
                {
                    targets: 'th-nip_bidan',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nip_bidan;
                    }
                },
                {
                    targets: 'th-nama_bidan',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_bidan;
                    }
                },
                {
                    targets: 'th-nama_desa',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_desa;
                    }
                },
                {
                    targets: 'th-nama_posyandu',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.nama_posyandu;
                    }
                },
                {
                    targets: 'th-telp_bidan',
                    className: "text-center",
                    render: function(data, type, row) {
                        return row.telp_bidan;
                    }
                },
                {
                    targets: 'th-aksi',
                    orderable: false,
                    className: "text-center",
                    render: function(data, type, row) {
                        return '<a href="javascript:;" onclick="page_script.edit(this)"\
                        data-id_posyandu="' + row.id_posyandu + '"\
                        data-nama_posyandu="' + row.nama_posyandu + '"\
                        data-nama_desa="' + row.nama_desa + '"\
                        data-nip_bidan="' + row.nip_bidan + '"\
                        data-nama_bidan="' + row.nama_bidan + '"\
                        data-telp_bidan="' + row.telp_bidan + '"\
                        data-tanggal_mulai="' + row.tanggal_mulai + '"\
                        data-tanggal_mulai_tugas="' + row.tanggal_mulai_tugas + '"\
                        class="btn btn-success mr-3"\
                        data-toggle="tooltip" data-placement="top" title="Edit"><i class="far fa-edit"></i></a>' +
                        '<a href="javascript:;" onclick="page_script.konfirmasi_hapus(this)"\
                        data-deleted="' + row.deleted_status + '"\
                        data-nip_bidan="' + row.nip_bidan + '"\
                        data-id_bidan="' + row.id_bidan + '"\
                        class="btn btn-'+ (row.deleted_status == 1 ? "success" : "danger") +'"\
                        data-toggle="tooltip" data-placement="top" title="'+ (row.deleted_status == 1 ? "non aktifkan" : "aktifkan") +'">\
                            <i class="'+ (row.deleted_status == 1 ? "fas fa-check" : "fas fa-ban") +'"></i>\
                        </a>';
                    }
                }
            ]
            page_script.data_table = $("#tabel").DataTable(setting);
        },
        konfirmasi_hapus: function(element) {
            var data = $(element).data();
            page_script.modal_hapus.find(".modal-title").html("Ubah status data " + data.id_bidan + " ?")
            page_script.modal_hapus.find(".positive").attr('onclick', "page_script.hapus('" + data.id_bidan + "', "+data.deleted+")")
            page_script.modal_hapus.modal('show');
        },
        simpan: function() {
            main.block()
            var data_send = {}
            data_send.edit = $("#edit").val()
            data_send.nip_bidan = $("#nip_bidan").val()
            data_send.nama_bidan = $("#nama_bidan").val()
            data_send.telp_bidan = $("#telp_bidan").val()
            data_send.id_posyandu = $("#id_posyandu").val()
            data_send.tanggal_mulai = $("#tanggal_mulai").val()
            data_send.tanggal_mulai_tugas = $("#tanggal_mulai_tugas").val()

            main.ajax("master_bidan/simpan",JSON.stringify(data_send), function(msg){
                var data = JSON.parse(msg);

                if(data.status_code != 200){
                    page_script.notifikasi("warning", "Simpan Gagal");
                }else{
                    if (data.status_data == -1) {
                        main.notifikasi("warning","Nip Bidan Sudah Ada");
                    }else{
                        main.notifikasi("success", "Data Tersimpan");
                    }
                    page_script.data_table.ajax.reload();
                    $("#modal_tambah").modal('hide');
                }
                main.unblock();
            });
        },
        hapus: function(id,deleted) {
            var data_send = {}
            data_send.id = id
            data_send.deleted = deleted
            main.ajax("master_bidan/delete",JSON.stringify(data_send), function(msg){
                var data = JSON.parse(msg);

                if(data.status_code != 200){
                    page_script.notifikasi("danger", "Ubah Gagal");
                }else{
                    main.notifikasi("warning", "Ubah Berhasil");
                    page_script.data_table.ajax.reload();
                    page_script.modal_hapus.modal('hide');
                }
                main.unblock();
            });
        },
        edit: function(element) {
            var data = $(element).data();
            page_script.modal_tambah.find("#nip_bidan").val(data.nip_bidan).attr('readonly',true);
            page_script.selectized_posyandu.addOption({
                id: data.id_posyandu,
                field: data.nama_posyandu + " (" +data.nama_desa+ ")"
            });
            page_script.selectized_posyandu.setValue(data.id_posyandu);
            page_script.modal_tambah.find("#nama_bidan").val(data.nama_bidan);
            page_script.modal_tambah.find("#telp_bidan").val(data.telp_bidan);
            page_script.date_tanggal_mulai.setDate(data.tanggal_mulai);
            page_script.date_tanggal_mulai_tugas.setDate(data.tanggal_mulai_tugas);

            page_script.modal_tambah.find("#edit").val("1");
            page_script.modal_tambah.modal('show');
        },
        reset_form: function() {
            page_script.modal_tambah.find("#nip_bidan").val(null).attr('readonly',false);
            page_script.modal_tambah.find("#nama_bidan").val(null);
            page_script.modal_tambah.find("#telp_bidan").val(null);
            page_script.modal_tambah.find("#edit").val(null);
            page_script.clear_select_posyandu();
        },
        clear_select_posyandu: function() {
            page_script.selectized_posyandu.clearOptions()
        },
        init: function() {
            page_script.load_data();
            page_script.modal_tambah = $("#modal_tambah");
            page_script.modal_hapus = $("#modal_hapus");
            $('#search-table').keyup(function() {
                var cari = $(this).val();
                main.delay(function() {
                    $("#tabel").dataTable().fnFilter(cari);
                }, 500);
            });

            $("#tambah").on('click', function() {
                page_script.reset_form();
                page_script.modal_tambah.modal('show');
            });

            $("#btn_mod_simpan").click(function(){
                if($('#form_tambah').valid())
                    page_script.simpan();
            });
            page_script.date_tanggal_mulai = flatpickr("#tanggal_mulai", {altInput: true, altFormat: "d-m-Y"});
            page_script.date_tanggal_mulai_tugas = flatpickr("#tanggal_mulai_tugas", {altInput: true, altFormat: "d-m-Y"});

            page_script.selectized_posyandu = main.selectize_remote("#id_posyandu", "master_posyandu/get_combobox/")[0].selectize
            page_script.modal_tambah.on('keyup', '#id_posyandu-selectized', function() {
                if ($(this).val().length == 0) {
                    page_script.clear_select_posyandu();
                    $(this).blur()
                    $(this).focus()
                }
            })
            page_script.modal_tambah.on('shown.bs.modal', function() {
                page_script.clear_select_posyandu()
                page_script.selectized_posyandu.blur()
            })
        }
    };
    $(document).ready(function() {
        main = Object.create(main_js);
        main.init("<?= BASE_URL ?>");
        page_script.init();
    });
</script>
